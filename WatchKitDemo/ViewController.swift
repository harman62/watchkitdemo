//
//  ViewController.swift
//  WatchKitDemo
//
//  Created by Harman Kaur on 2019-10-15.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import UIKit
import WatchConnectivity    //built in library for making Phone watch communication work

class ViewController: UIViewController, WCSessionDelegate {
    
    //built-in method for dealing with communication
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    @IBOutlet weak var sendMsgOutputLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    var phoneCounter:Int = 0
    var messageCounter: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("---PHONE APP LOADED---")
        
        //@TODO: Does the watch support communication with the phone
        
        if (WCSession.isSupported() == true) {
            sendMsgOutputLabel.text = "WatchConnectivity is supported!"
            //activate the session
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            sendMsgOutputLabel.text = "WatchConnectivity is not supported!"
        }
        
    }

    //MARK: custom functions
    //------------

    @IBAction func phoneButtonPressed(_ sender: UIButton) {
        print("Phone button pressed")
        self.phoneCounter = self.phoneCounter + 1
        self.counterLabel.text = "Counter: \(self.phoneCounter)"
        print(self.counterLabel)
    }
    
    @IBAction func sendMsgButton(_ sender: UIButton) {
        print("sending message to the watch")
        
        if(WCSession.default.isReachable == true) {
            let message = ["name": "Harman"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            messageCounter = messageCounter + 1
            sendMsgOutputLabel.text = "Message sent \(messageCounter)"
        }
        else {
            messageCounter = messageCounter + 1
            sendMsgOutputLabel.text = "Cant reach watch \(messageCounter)"
        }
       
    }
}

