//
//  Page2InterfaceController.swift
//  WatchKitDemo WatchKit Extension
//
//  Created by Harman Kaur on 2019-10-15.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import WatchKit
import Foundation


class Page2InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("welcome to page 2")
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
