//
//  InterfaceController.swift
//  WatchKitDemo WatchKit Extension
//
//  Created by Harman Kaur on 2019-10-15.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity  //need this to link phn nd watch


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    
    //MARK: Outlets
    //label for output
    @IBOutlet weak var outputLabel: WKInterfaceLabel!
    //outlet for phone message
    @IBOutlet weak var phoneMsgLabel: WKInterfaceLabel!
    
    var count:Int = 0
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("---WATCH APP LOADED")
        
        //@TODO: Does the watch support communication with the phone
        
        if (WCSession.isSupported() == true) {
            phoneMsgLabel.setText("WC supported!")
            //activate the session
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            phoneMsgLabel.setText("WC not supported!")
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: Custom functions
    
    @IBAction func watchButtonPressed() {
        print("I clicked the button")
        self.count = self.count + 1
        self.outputLabel.setText("Welcome!! \(self.count)")
    }
    
}
